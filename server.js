const mathModule = require('./components/math'); // './math' refers to the relative path of the math.js file

const sum = mathModule.add(5, 3);
const difference = mathModule.subtract(10, 4);
const square =  mathModule.square(6)

console.log(`Sum: ${sum}`);
console.log(`Difference: ${difference}`)
console.log(`Square: ${square}`)
